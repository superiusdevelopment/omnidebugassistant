<?php

use Illuminate\Support\Facades\Route;
use Superius\OmniDebugAssistant\Http\Controllers\TelescopeController;

Route::prefix('a2a/dev/telescope')->middleware('auth.local')->group(function () {
    Route::post('/whitelist/add-ip-address', [TelescopeController::class, 'addIpAddressOnWhitelist']);
    Route::post('/whitelist/delete', [TelescopeController::class, 'removeWhitelist']);

    Route::post('/entries/enable', [TelescopeController::class, 'enableTelescopeEntriesTags']);
    Route::post('/entries/disable', [TelescopeController::class, 'disableTelescopeEntriesTags']);
});
