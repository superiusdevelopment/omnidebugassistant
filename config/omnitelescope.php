<?php

return [
    'allowed_ip_range' => [
        'us' => env('ALLOWED_IP_RANGE_US', '127.0.0.1'),
    ],
];
