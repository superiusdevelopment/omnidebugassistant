<?php

namespace Superius\OmniDebugAssistant\Enums;

enum TelescopeFileNameEnum: string
{
    case WHITE_LIST_FILE = 'telescope_ip_whitelist';
    case TELESCOPE_ENABLE_ENTRIES = 'telescope_enable_entries';
}
