<?php

namespace Superius\OmniDebugAssistant\Commands;

use Exception;
use Illuminate\Console\Command;

use Illuminate\Support\Facades\Storage;
use Superius\OmniDebugAssistant\Enums\TelescopeFileNameEnum;

class TelescopeState extends Command
{
    protected $signature = 'omnihub:telescope {state}';
    protected $description = 'Command enables or disables incoming entries and stop showing them on Telescope App';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $flag = $this->argument('state');

        if ($flag === 'enable') {
            try {
                if (Storage::exists(TelescopeFileNameEnum::TELESCOPE_ENABLE_ENTRIES->value)) {
                    $this->comment('Telescope is already enabled.');

                    return 0;
                }

                Storage::append(TelescopeFileNameEnum::TELESCOPE_ENABLE_ENTRIES->value, '');

                $this->info('Telescope is now enabled.');
            } catch (Exception $e) {
                $this->error('Failed to make telescope enabled.');

                $this->error($e->getMessage());

                return 1;
            }
        }

        if ($flag === 'disable') {
            try {
                if (Storage::missing(TelescopeFileNameEnum::TELESCOPE_ENABLE_ENTRIES->value)) {
                    $this->comment('Telescope is already disabled.');

                    return 0;
                }

                Storage::delete(TelescopeFileNameEnum::TELESCOPE_ENABLE_ENTRIES->value);

                $this->info('Telescope is now disabled.');
            } catch (Exception $e) {
                $this->error('Failed to make telescope disabled.');

                $this->error($e->getMessage());

                return 1;
            }
        }

        return 0;
    }
}
