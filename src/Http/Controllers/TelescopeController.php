<?php

namespace Superius\OmniDebugAssistant\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Superius\OmniDebugAssistant\Enums\TelescopeFileNameEnum;
use Superius\OmniDebugAssistant\Requests\CreateTelescopeWhitelistRequest;

class TelescopeController extends Controller
{
    public function addIpAddressOnWhitelist(CreateTelescopeWhitelistRequest $request): JsonResponse
    {
        $ip_address = $request->get('ip_address') ?: Request::ip();

        if (Storage::exists(TelescopeFileNameEnum::WHITE_LIST_FILE->value)) {
            if (Str::contains(Storage::get(TelescopeFileNameEnum::WHITE_LIST_FILE->value), $ip_address)) {
                return response()->json(['message' => 'IP address already exists on the list!']);
            }

            Storage::append(TelescopeFileNameEnum::WHITE_LIST_FILE->value, $ip_address);

            return response()->json(['message' => 'IP address successfully appended on the list!']);
        }
        Storage::put(TelescopeFileNameEnum::WHITE_LIST_FILE->value, $ip_address);

        return response()->json([
            'message' => 'File ' . TelescopeFileNameEnum::WHITE_LIST_FILE->value .
                ' successfully created and IP address ' . $ip_address .
                ' added on the list!',
        ]);
    }

    public function removeWhitelist(): JsonResponse
    {
        if (Storage::exists(TelescopeFileNameEnum::WHITE_LIST_FILE->value)) {
            Storage::delete(TelescopeFileNameEnum::WHITE_LIST_FILE->value);

            return response()->json(['message' => 'File ' . TelescopeFileNameEnum::WHITE_LIST_FILE->value .
                ' successfully deleted!',
            ]);
        }

        return response()->json(['message' => 'File ' . TelescopeFileNameEnum::WHITE_LIST_FILE->value .
            ' does not exist!',
        ]);
    }

    public function enableTelescopeEntriesTags(): JsonResponse
    {
        if (Storage::exists(TelescopeFileNameEnum::TELESCOPE_ENABLE_ENTRIES->value)) {
            return response()->json([
                'message' => 'File ' . TelescopeFileNameEnum::TELESCOPE_ENABLE_ENTRIES->value .
                    ' already exists in storage folder!',
            ]);
        }

        Storage::append(TelescopeFileNameEnum::TELESCOPE_ENABLE_ENTRIES->value, '');

        return response()->json(['message' => 'File ' . TelescopeFileNameEnum::TELESCOPE_ENABLE_ENTRIES->value .
            ' successfully created!',
        ]);
    }

    public function disableTelescopeEntriesTags(): JsonResponse
    {
        if (Storage::exists(TelescopeFileNameEnum::TELESCOPE_ENABLE_ENTRIES->value)) {
            Storage::delete(TelescopeFileNameEnum::TELESCOPE_ENABLE_ENTRIES->value);

            return response()->json(['message' => 'File ' . TelescopeFileNameEnum::TELESCOPE_ENABLE_ENTRIES->value .
                ' successfully deleted!',
            ]);
        }

        return response()->json(['message' => 'File ' . TelescopeFileNameEnum::TELESCOPE_ENABLE_ENTRIES->value .
            ' does not exist!',
        ]);
    }
}
