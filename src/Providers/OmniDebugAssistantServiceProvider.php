<?php

namespace Superius\OmniDebugAssistant\Providers;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use Superius\OmniDebugAssistant\Commands\TelescopeState;
use Illuminate\Support\Facades\File;

class OmniDebugAssistantServiceProvider extends ServiceProvider
{

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //scripts that prepares build
        if ($this->app->runningInConsole()) {
            $this->commands([
                TelescopeState::class
            ]);
        }

        $this->mergeConfigFrom(
            __DIR__.'/../../config/log-viewer.php', 'log-viewer'
        );

        if ($this->app->environment('local')) {
            $source = base_path('vendor/superius/omni-debug-assistant/config/omnitelescope.php');
            $destination = base_path('config/omnitelescope.php');

            File::copy($source, $destination);
        }
    }

    public function boot()
    {
        $this->registerRoutes();
        $this->callAfterResolving(Schedule::class, function (Schedule $schedule) {
            $schedule->command('telescope:prune --hours=3')->everyThreeHours();
            $schedule->command('omnihub:telescope disable')->everyThreeHours();
        });
    }

    protected function registerRoutes(): void
    {
        Route::group([
            'prefix' => 'api',
        ], function () {
            $this->loadRoutesFrom(__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' .
                DIRECTORY_SEPARATOR . 'routes' . DIRECTORY_SEPARATOR . 'api.php');
        });
    }
}
