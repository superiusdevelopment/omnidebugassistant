<?php

namespace Superius\OmniDebugAssistant\Providers;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Laravel\Telescope\IncomingEntry;
use Laravel\Telescope\Telescope;
use Laravel\Telescope\TelescopeApplicationServiceProvider;
use Superius\OmniDebugAssistant\Commands\TelescopeState;
use Superius\OmniDebugAssistant\Enums\TelescopeFileNameEnum;

class TelescopeServiceProvider extends TelescopeApplicationServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register(): void
    {
        // Telescope::night();

        $this->hideSensitiveRequestDetails();

        Telescope::filter(function (IncomingEntry $entry) {
            if (
                $this->app->environment('local') ||
                Storage::exists(TelescopeFileNameEnum::TELESCOPE_ENABLE_ENTRIES->value)
            ) {
                return true;
            }

            return $entry->isReportableException() ||
                $entry->isFailedRequest() ||
                $entry->isFailedJob() ||
                $entry->isScheduledTask() ||
                $entry->hasMonitoredTag();
        });
    }

    /**
     * Prevent sensitive request details from being logged by Telescope.
     *
     * @return void
     */
    protected function hideSensitiveRequestDetails(): void
    {
        if ($this->app->environment('local')) {
            return;
        }

        Telescope::hideRequestParameters(['_token']);

        Telescope::hideRequestHeaders([
            'cookie',
            'x-csrf-token',
            'x-xsrf-token',
        ]);
    }

    /**
     * Register the Telescope gate.
     *
     * This gate determines who can access Telescope in non-local environments.
     *
     * @return void
     */
    protected function gate(): void
    {
        Gate::define('viewTelescope', function ($user = null) {
            $userIp = Request::ip();

            if (Str::startsWith($userIp, config('omnitelescope.allowed_ip_range.us'))) {
                Storage::put(TelescopeFileNameEnum::TELESCOPE_ENABLE_ENTRIES->value, '');
                return true;
            }

            $whitelistFile = TelescopeFileNameEnum::WHITE_LIST_FILE->value;
            if (Storage::exists($whitelistFile)) {
                return Str::contains(Storage::get($whitelistFile), $userIp);
            }

            if (($user && $user->isAdmin()) ||
                data_get($_SERVER, 'REMOTE_ADDR') === config('omnihub.debug.remote_address')) {
                return true;
            }

            return false;
        });
    }
}
