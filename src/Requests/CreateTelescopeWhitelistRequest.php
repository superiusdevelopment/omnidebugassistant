<?php

namespace Superius\OmniDebugAssistant\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateTelescopeWhitelistRequest extends FormRequest
{
    /**
     * @return array<string,array<mixed>>
     */
    public function rules(): array
    {
        return [
            'ip_address' => ['sometimes', 'nullable', 'string', 'ip'],
        ];
    }
}
